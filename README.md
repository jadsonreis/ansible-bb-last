### Execução das Roles:
* O inventário Ansible está definido no arquivo  **hosts-itoa-bb/hosts-itoa**
* As variáveis são definidas em **hosts-itoa-bb/hosts-itoa**, quando intencionalmente Globais. Ou definidas nas pastas **vars** de cada **role**.

**Exemplo 1** - Criação de usuário local ```itoausr``` em todos os servidores do inventário com troca de chaves rsa.

```
$ ansible-playbook -i hosts ../roles/00exec-generic-roles.yml -e "hosts=all role=create-user"
````
**Exemplo 2** - Instalação do ```Squid``` no servidor indicado no grupo ***squidProxy*** do inventário.

```
$ ansible-playbook -i hosts ../roles/00exec-generic-roles.yml -e "hosts=squidProxy role=squid"
```
----------------------
